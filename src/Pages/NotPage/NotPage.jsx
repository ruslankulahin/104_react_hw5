import { Link } from "react-router-dom";
import "./NotPage.scss";

export default function NotPage() {
    return (
        <div className="container">
            <div className="not-page">
                <div>
                    <p className="page-title">Ця сторінка була викрадена</p>
                    <p className="page-desc">
                        Давайте повернемося на головну сторінку і спробуємо ще раз.
                        <br />
                        <br />
                        Ви обов`язково знайдете те що шукаєте :) {" "}
                    </p>
                    <p>
                        <Link to="/">Головна</Link>
                    </p>
                </div>
            </div>
        </div>
        
    );
}
