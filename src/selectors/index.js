export const selectProducts = (state) => state.app.products;
export const selectIsLoading = (state) => state.app.IsLoading;
export const selectConfirmationModal = (state) =>
    state.app.showConfirmationModal;
export const selectFavoritesItems = (state) => state.favorites.favoriteItems;
export const selectFavoritesModal = (state) =>
    state.favorites.showFavoritesModal;
export const selectCartItems = (state) => state.cart.cartItems;
export const selectCartModal = (state) => state.cart.showCartModal;
