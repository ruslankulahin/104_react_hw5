import { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { Route, Routes } from "react-router-dom";
import ProductList from "./components/ProductList";
import Modal from "./components/Modal";
import Button from "./components/Button";
import Header from "./components/Header";
import Footer from "./components/Footer";
import NotPage from "./Pages/NotPage/index.js";
import PageCart from "./Pages/PageCart/index.js";
import PageFavorites from "./Pages/PageFavorites/index.js";
import { useDispatch, useSelector } from "react-redux";
import {
    selectProducts,
    selectFavoritesItems,
    selectFavoritesModal, 
    selectCartItems,
    selectCartModal,
    selectConfirmationModal,

    } from "./selectors/index.js";
import { actionsShowFavoritesModal } from "./store/favorites.slice.js";
import { actionFetchProducts, actionsShowConfirmationModal } from "./store/app.slice.js";
import { 
    actionAddToCart, 
    actionRemoveFromCart, 
    actionsRemoveItemFromCart, 
    actionsShowCartModal 
} from "./store/cart.slice.js";
import "./App.scss";

export default function App() {
    const [itemToRemove, setItemToRemove] = useState([]); 

    const dispatch = useDispatch();

    const products = useSelector(selectProducts);
    const favoriteItems = useSelector(selectFavoritesItems);
    const showFavoritesModal = useSelector(selectFavoritesModal);
    const cartItems = useSelector(selectCartItems);
    const showCartModal = useSelector(selectCartModal);
    const showConfirmationModal = useSelector(selectConfirmationModal);

    useEffect(() => {
        dispatch(actionFetchProducts());
    }, []);

    const handleToggleCartModal = () => {
        dispatch(actionsShowCartModal());
    };

    const handleOutsideCartModalClick = (event) => {
        if (!event.target.closest(".modal")) {
            handleToggleCartModal();
        }
    };


    const handleToggleFavoritesModal = () => {
        dispatch(actionsShowFavoritesModal());
    };

    const handleOutsideFavoritesModalClick = (event) => {
        if (!event.target.closest(".modal")) {
            handleToggleFavoritesModal();
        }
    };
    

    const handleOutsideConfirmationModalClick = (event) => {
        if (!event.target.closest(".modal")) {
            dispatch(actionsShowConfirmationModal());
        }
    };


    const handleRemoveClick = (item) => {
        setItemToRemove(item);
        dispatch(actionsShowConfirmationModal());
    };

    const handlerConfirmRemove = () => {
        if (itemToRemove) {
            dispatch(actionsRemoveItemFromCart(itemToRemove));
        }
        dispatch(actionsShowConfirmationModal());
    };

    const cartItemTotal = cartItems.reduce(
        (total, item) => total + item.quantity,
        0
    );
    const totalPrice = cartItems.reduce(
        (total, item) => total + item.price * item.quantity,
        0
    );

    return (
        <div className="app-wrapper">
            <Header cartItemTotal={cartItemTotal} />
            <Routes>
                <Route path="/" element={<ProductList products={products} />}/>
                <Route path="/favorites" element={<PageFavorites />}/>
                <Route path="/cart" element={<PageCart handleRemoveClick={handleRemoveClick}/>}/>
                <Route path="*" element={<NotPage />} />
            </Routes>
            <Footer />

            {showCartModal && (
                <Modal
                    classNames="shoping-cart__modal"
                    closeButton={handleToggleCartModal}
                    closeModal={handleToggleCartModal}
                    handleOutsideClick={handleOutsideCartModalClick}
                    header="Кошик"
                    text={!cartItems.length && <div>Ваш кошик порожній.</div>}
                    actions={cartItems.map((item) => (
                        <div className="item-wrapper__modal" key={item.id}>
                            <div className="item-text__modal">
                                <div className="item-name__modal">
                                    {item.name}
                                </div>
                                <div className="item-price__modal">
                                    {item.price} грн
                                </div>
                            </div>
                            <div className="add-remove-btn__wrapper">
                                <Button
                                    className="btn minus-from-cart"
                                    onClick={() => dispatch(actionRemoveFromCart(item))}
                                >
                                    -
                                </Button>
                                <div className="quantity-product">
                                    {item.quantity}
                                </div>
                                <Button
                                    className="btn plus-to-cart"
                                    onClick={() => dispatch(actionAddToCart(item))}
                                >
                                    +
                                </Button>
                                <Button
                                    className="btn close-item__modal"
                                    onClick={() => dispatch(actionsRemoveItemFromCart(item))}
                                >
                                    &times;
                                </Button>
                            </div>
                        </div>
                    ))}
                    totalPrice={
                        cartItems.length && (
                            <div className="total-price__modal">
                                Разом: {totalPrice} грн
                            </div>
                        )
                    }
                />
            )}
            {showFavoritesModal && (
                <Modal
                    classNames="favorites__modal"
                    closeButton={handleToggleFavoritesModal}
                    closeModal={handleToggleFavoritesModal}
                    handleOutsideClick={handleOutsideFavoritesModalClick}
                    header="Обрані"
                    text={
                        !favoriteItems.length && (
                            <div>У вас ще немає улюблених товарів.</div>
                        )
                    }
                    actions={favoriteItems.map((item) => (
                        <div className="item-wrapper__modal" key={item.id}>
                            <div className="item-text__modal">
                                <div className="item-name__modal">
                                    {item.name}
                                </div>
                                <div className="item-price__modal">
                                    {item.price} грн
                                </div>
                            </div>
                            <Button
                                className="btn__favorite-modal"
                                onClick={() => {
                                    dispatch(actionAddToCart(item));
                                }}
                            >
                                Додати до кошика
                            </Button>
                        </div>
                    ))}
                    // totalPrice=""
                />
            )}

            {showConfirmationModal && (
                <Modal
                    header="Ви бажаєте видалити цей товар з кошику?"
                    closeButton={true}
                    closeModal={() => dispatch(actionsShowConfirmationModal())}
                    handleOutsideClick={(event) =>
                        handleOutsideConfirmationModalClick(event)
                    }
                    text={
                        <p>
                            Може він вам все ще потрібний?
                            <br />
                            Ви впевнені, що бажаєте його видалити?
                        </p>
                    }
                    actions={
                        <div className="button-container">
                            <Button
                                classNames={"btn__delete-modal"}
                                onClick={(itemToRemove) =>
                                    handlerConfirmRemove(itemToRemove)
                                }
                            >
                                Так
                            </Button>
                            <Button
                                className="btn btn__delete-modal"
                                onClick={() => dispatch(actionsShowConfirmationModal())}
                            >
                                Ні
                            </Button>
                        </div>
                    }
                />
            )}
        </div>
    );
}

App.propTypes = {
    products: PropTypes.array,
    cartItems: PropTypes.array,
    favoriteItems: PropTypes.array,
    AddToCart: PropTypes.func,
    RemoveFromCart: PropTypes.func,
    AddToFavorites: PropTypes.func,
    showCartModal: PropTypes.func,
    ItemCartClose: PropTypes.func,
    classNames: PropTypes.string,
    closeButton: PropTypes.func,
    closeModal: PropTypes.bool,
    handleOutsideClick: PropTypes.func,
    handleToggleFavoritesModal: PropTypes.func,
    handleAddToCart: PropTypes.func,
    header: PropTypes.object,
    text: PropTypes.any,
    totalPrice: PropTypes.number,
};
