import { configureStore } from "@reduxjs/toolkit";
import thunk from "redux-thunk";
import logger from "redux-logger";
import rootReducers from "./app.slice.js";
import favoritesReducers from "./favorites.slice.js";
import cartReducers from "./cart.slice.js";

export default configureStore({
    reducer: {
        app: rootReducers,
        favorites: favoritesReducers,
        cart: cartReducers,
    },
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware().concat(thunk, logger),
    devTools: process.env.NODE_ENV !== "production",
});
