import PropTypes from "prop-types";
import Button from "../Button";
import { faStar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useDispatch, useSelector } from "react-redux";
import {
    selectFavoritesItems,
    selectCartItems,
} from "../../selectors/index.js";
import { actionAddFavorites } from "../../store/favorites.slice.js";
import {
    actionAddToCart,
    actionsShowCartModal,
} from "../../store/cart.slice.js";
import "./ProductCard.scss";

export default function ProductCard({ product }) {
    const { name, price, image } = product;

    const dispatch = useDispatch();
    const favoriteItems = useSelector(selectFavoritesItems);
    const cartItems = useSelector(selectCartItems);

    const inFavorites = (product) => {
        if (favoriteItems.some((item) => item.id === product.id)) {
            return true;
        }
        return false;
    };

    const inCart = (product) => {
        if (cartItems.some((item) => item.id === product.id)) {
            return true;
        }
        return false;
    };

    return (
        <div className="product-card">
            <div className="image-wrapper">
                <img src={image} alt={name} />
            </div>
            <h3 className="product-name">{name}</h3>
            <div className="card-footer">
                {!inFavorites(product) && (
                    <Button
                        className="add-favorite"
                        onClick={() => dispatch(actionAddFavorites(product))}
                    >
                        <FontAwesomeIcon
                            className="icon-star"
                            icon={faStar}
                            size="xs"
                            style={{
                                color: "#9a9a9c",
                            }}
                        />
                    </Button>
                )}
                {inFavorites(product) && (
                    <Button
                        className="add-favorite"
                        style={{
                            backgroundColor: "#dddddd",
                            borderRadius: "60px",
                        }}
                        onClick={() => dispatch(actionAddFavorites(product))}
                    >
                        <FontAwesomeIcon
                            icon={faStar}
                            size="xs"
                            style={{ color: "#f4f88b" }}
                        />
                    </Button>
                )}
                <p className="product-price">{price} грн</p>
            </div>
            {!inCart(product) && (
                <Button
                    className="add-to-cart"
                    onClick={() => dispatch(actionAddToCart(product))}
                >
                    Додати до кошика
                </Button>
            )}
            {inCart(product) && (
                <Button
                    className="remove-to-cart"
                    onClick={() => {
                        dispatch(actionsShowCartModal(product));
                    }}
                >
                    Товар у кошику
                </Button>
            )}
        </div>
    );
}

ProductCard.propTypes = {
    product: PropTypes.shape({
        name: PropTypes.string,
        price: PropTypes.number,
        image: PropTypes.string,
    }),
    children: PropTypes.any,
    inCart: PropTypes.bool,
    inFavorites: PropTypes.bool,
    AddToCart: PropTypes.func,
    AddToFavorites: PropTypes.func,
    showCartModal: PropTypes.func,
};
