import PropTypes from "prop-types";
import ProductCard from "../ProductCard";
import "./ProductList.scss";

export default function ProductList({products}) {

    return (
        <div className="container">
            <div className="product-list">
                {products.map((product) => (
                    <ProductCard
                        key={product.id}
                        product={product}
                    />
                ))}
            </div>
        </div>
    );
}

ProductList.propTypes = {
    products: PropTypes.array,
    cartItems: PropTypes.array,
    favoriteItems: PropTypes.array,
    AddToCart: PropTypes.func,
    AddToFavorites: PropTypes.func,
    showCartModal: PropTypes.func,
    inCart: PropTypes.bool,
    inFavorites: PropTypes.bool,
};
