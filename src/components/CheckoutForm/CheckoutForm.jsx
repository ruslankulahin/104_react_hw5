import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useFormik } from "formik";
import { PatternFormat } from "react-number-format";
import { actionsClearCart } from "../../store/cart.slice.js";
import { selectCartItems } from "../../selectors/index.js";
import { validationSchema } from "./validation.js";
import InputBox from "../Form/Input/Input.jsx";
import Button from "../Button/Button.jsx";
import "./CheckoutForm.scss";

const CheckoutForm = () => {
    const dispatch = useDispatch();
    const cartItems = useSelector(selectCartItems);

    const formik = useFormik({
        initialValues: {
            firstName: "",
            lastName: "",
            age: "",
            address: "",
            phone: "",
        },
        validationSchema,

        onSubmit: (values) => {
            console.log("Придбані товари:", cartItems);
            console.log("Інформація з форми:", values);
            dispatch(actionsClearCart(cartItems));
        },
    });

    return (
        <form className="checkout-form" onSubmit={formik.handleSubmit}>
            <div className="checkout-form__row">
                <div className="form-group">
                    <InputBox
                        type="text"
                        name={"firstName"}
                        label={"Ім'я"}
                        classNames={"input-box"}
                        error={
                            formik.errors.firstName && formik.touched.firstName
                        }
                        errorMessage={formik.errors.firstName}
                        {...formik.getFieldProps("firstName")}
                    />
                </div>
                <div className="form-group">
                    <InputBox
                        type="text"
                        name={"lastName"}
                        label={"Прізвище"}
                        classNames={"input-box"}
                        error={
                            formik.errors.lastName && formik.touched.lastName
                        }
                        errorMessage={formik.errors.lastName}
                        {...formik.getFieldProps("lastName")}
                    />
                </div>
                <div className="form-group">
                    <InputBox
                        type="text"
                        name={"age"}
                        label={"Вік"}
                        classNames={"input-box"}
                        error={formik.errors.age && formik.touched.age}
                        errorMessage={formik.errors.age}
                        {...formik.getFieldProps("age")}
                    />
                </div>
            </div>
            <div className="checkout-form__row">
                <div className="form-group">
                    <InputBox
                        type="text"
                        name={"address"}
                        label={"Адреса доставки"}
                        classNames={"input-box"}
                        error={formik.errors.address && formik.touched.address}
                        errorMessage={formik.errors.address}
                        {...formik.getFieldProps("address")}
                    />
                </div>
                <div className="form-group">
                    <label className="form-item input-box">
                        <p className="form-label">Мобільний телефон</p>
                        <PatternFormat
                            // імпортуємо PatternFormat з бібліотеки 'react-number-format'
                            // і використовуємо його для поля мобільного телефону.
                            // Ми встановлюємо format на (###)###-##-##,
                            //  та mask на _, що позначає, де будуть відображені цифри.
                            format="(###)###-##-##"
                            mask="_"
                            type="tel"
                            id="phone"
                            name="phone"
                            value={formik.values.phone}
                            onBlur={formik.handleBlur}
                            onValueChange={(values) => {
                                formik.setFieldValue("phone", values.value);
                            }}
                        />
                        {formik.touched.phone && formik.errors.phone ? (
                            <div className="error-message">
                                {formik.errors.phone}
                            </div>
                        ) : null}
                    </label>
                </div>
                <div className="btn-checkout__wrapper">
                    <Button type={"submit"} classNames="btn-checkout">
                        Checkout
                    </Button>
                </div>
            </div>
        </form>
    );
};

export default CheckoutForm;
